package pobj.pinboard.editor;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import pobj.pinboard.document.Board;
import pobj.pinboard.document.Clip;

public class Selection {
	private List<Clip> clipsSelectionnes = new ArrayList<Clip>();

	public void select(Board board, double x, double y) {
		clear();
		Clip clipAjoute = null;
		for (Clip clip : board.getContents()) {
			if (clip.isSelected(x, y)) {
				clipAjoute=clip;
			}
		}
		if(!(clipAjoute==null))
			clipsSelectionnes.add(clipAjoute);
	}
	
	public void toogleSelect(Board board, double x, double y) {
		Clip clipAjoute = null;
		for (Clip clip : board.getContents()) {
			if (clip.isSelected(x, y)) {
				clipAjoute = clip;
			}
		}
		if(!(clipAjoute==null))
		{
			if(clipsSelectionnes.contains(clipAjoute))
				clipsSelectionnes.remove(clipAjoute);
			else
			{
				clipsSelectionnes.add(clipAjoute);		
			}
		}
	}
	
	public void clear() {
		clipsSelectionnes.clear();
	}
	
	public List<Clip> getContents(){
		return clipsSelectionnes;
	}
	
	public void drawFeedBack(GraphicsContext gc) {
		for(Clip clip : clipsSelectionnes) {
			double left = clip.getLeft();
			double top = clip.getTop();
			double right = clip.getRight();
			double bottom = clip.getBottom();
			gc.setStroke(Color.BLUE);
			gc.strokeRect(left, top, right-left, bottom-top);
			
		}
	}
}
