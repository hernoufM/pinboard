package pobj.pinboard.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import pobj.pinboard.document.Board;
import pobj.pinboard.document.Clip;
import pobj.pinboard.document.ClipImage;
import pobj.pinboard.editor.commands.CommandAdd;
import pobj.pinboard.editor.commands.CommandGroup;
import pobj.pinboard.editor.commands.CommandRemove;
import pobj.pinboard.editor.commands.CommandUngroup;
import pobj.pinboard.editor.tools.Tool;
import pobj.pinboard.editor.tools.ToolEllipse;
import pobj.pinboard.editor.tools.ToolImage;
import pobj.pinboard.editor.tools.ToolRect;
import pobj.pinboard.editor.tools.ToolSelection;

public class EditorWindow implements EditorInterface, ClipboardListener {
	private Board board = new Board();
	private Tool outilCourante = new ToolRect();
	private Selection selection = new Selection();
	private CommandStack pileCommande = new CommandStack();
	private MenuItem pasteMenuItem;
	private MenuItem undoMenuItem;
	private MenuItem redoMenuItem;

	public EditorWindow(Stage stage) {
		stage.setTitle("PinBoard");
		VBox vbox = new VBox();
		Label stat = new Label("Filled red box tool");
		Canvas canvas = new Canvas(500, 500);
		Clipboard.getInstance().addListener(this);

		// MenuBar

		// Menu "File"
		Menu fileMenu = new Menu("File");
		MenuItem nouvelle = new MenuItem("New");
		nouvelle.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				new EditorWindow(new Stage());
			}
		});
		MenuItem close = new MenuItem("Close");
		close.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Clipboard.getInstance().removeListener(EditorWindow.this);
				stage.close();
			}
		});
		MenuItem open = new MenuItem("Open");
		open.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Ouvrir un board");
				fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Board file", "*.board"));
				File selectedFile = fileChooser.showOpenDialog(stage);
				if (selectedFile != null) {
					ObjectInputStream ois=null;
					try {
						ois = new ObjectInputStream(new FileInputStream(selectedFile.getAbsolutePath()));
						board = (Board) ois.readObject();
						for(Clip clip : board.getContents()) {
							if(clip instanceof ClipImage) {
								((ClipImage) clip).recalculerImage();
							}
						}
						selection.clear();
						pileCommande = new CommandStack();
						updateUndoRedoItems();
						board.draw(canvas.getGraphicsContext2D());
						stat.textProperty().set("Board " + selectedFile.getAbsolutePath()+" is opened");
					} catch (Exception e) {
						stat.textProperty().set("Error while opening board");
						e.printStackTrace();
					}
					finally {
						try {
							ois.close();
						} catch (IOException e) {
							stat.textProperty().set("Error while opening board");
						}
					}
				}
			}
		});
		MenuItem save = new MenuItem("Save");
		save.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Enregistrer le board");
				fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Board file", "*.board"));
				File selectedFile = fileChooser.showSaveDialog(stage);
				if (selectedFile != null) {
					ObjectOutputStream oos=null;
					try {
						oos = new ObjectOutputStream(new FileOutputStream(selectedFile.getAbsolutePath()));
						oos.writeObject(board);
					} catch (Exception e) {
						stat.textProperty().set("Error while opening board");
					}
					finally {
						try {
							oos.close();
						} catch (IOException e) {
							stat.textProperty().set("Error while opening board");
						}						
					}
					stat.textProperty().set("Board is saved as " + selectedFile.getAbsolutePath());
				}
			}
		});
		fileMenu.getItems().addAll(nouvelle, close, open, save);

		// Menu "Edit"
		Menu editMenu = new Menu("Edit");
		MenuItem copy = new MenuItem("Copy");
		copy.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Clipboard clipboard = Clipboard.getInstance();
				clipboard.clear();
				clipboard.copyToClipboard(selection.getContents());
				selection.clear();
				board.draw(canvas.getGraphicsContext2D());
				stat.textProperty().set("Copied");
			}
		});
		pasteMenuItem = new MenuItem("Paste");
		pasteMenuItem.setDisable(Clipboard.getInstance().isEmpty());
		pasteMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				Clipboard clipboard = Clipboard.getInstance();
				CommandAdd commande = new CommandAdd(EditorWindow.this, clipboard.copyFromClipboard());
				commande.execute();
				pileCommande.addCommand(commande);
				updateUndoRedoItems();
				selection.clear();
				board.draw(canvas.getGraphicsContext2D());
				stat.textProperty().set("Pasted");
			}
		});
		MenuItem delete = new MenuItem("Delete");
		delete.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (!selection.getContents().isEmpty()) {
					CommandRemove commande = new CommandRemove(EditorWindow.this, selection.getContents());
					commande.execute();
					pileCommande.addCommand(commande);
					updateUndoRedoItems();
					selection.clear();
				}
				board.draw(canvas.getGraphicsContext2D());
				stat.textProperty().set("Deleted");
			}
		});
		MenuItem group = new MenuItem("Group");
		group.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (!selection.getContents().isEmpty()) {
					CommandGroup commande = new CommandGroup(EditorWindow.this, selection.getContents());
					commande.execute();
					pileCommande.addCommand(commande);
					updateUndoRedoItems();
					selection.clear();
					board.draw(canvas.getGraphicsContext2D());
					stat.textProperty().set("Grouped");
				}
			}
		});
		MenuItem ungroup = new MenuItem("Ungroup");
		ungroup.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (!selection.getContents().isEmpty()) {
					CommandUngroup commande = new CommandUngroup(EditorWindow.this, selection.getContents());
					commande.execute();
					pileCommande.addCommand(commande);
					updateUndoRedoItems();
					selection.clear();
					board.draw(canvas.getGraphicsContext2D());
					stat.textProperty().set("Ungrouped");
				}
			}
		});
		undoMenuItem = new MenuItem("Undo");
		undoMenuItem.setDisable(pileCommande.isUndoEmpty());
		undoMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				pileCommande.undo();
				updateUndoRedoItems();
				selection.clear();
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		redoMenuItem = new MenuItem("Redo");
		redoMenuItem.setDisable(pileCommande.isRedoEmpty());
		redoMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				pileCommande.redo();
				updateUndoRedoItems();
				selection.clear();
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		editMenu.getItems().addAll(copy, pasteMenuItem, delete, group, ungroup, undoMenuItem, redoMenuItem);

		// Menu "Tools"
		Menu toolsMenu = new Menu("Tools");
		MenuItem toolBox = new MenuItem("Rectangle");
		toolBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				outilCourante = new ToolRect();
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
			}
		});
		MenuItem toolEllipse = new MenuItem("Ellipse");
		toolEllipse.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				outilCourante = new ToolEllipse();
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
			}
		});
		MenuItem toolImage = new MenuItem("Image...");
		toolImage.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				imageAction(stage, stat);
			}
		});
		MenuItem toolSelection = new MenuItem("Selection");
		toolSelection.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				outilCourante = new ToolSelection();
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
			}
		});
		toolsMenu.getItems().addAll(toolBox, toolEllipse, toolImage, toolSelection);

		MenuBar menubar = new MenuBar(fileMenu, editMenu, toolsMenu);

		// ToolBar

		// Button "Box"
		Button box = new Button("Box");
		box.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				outilCourante = new ToolRect();
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
			}
		});
		// Button "Ellipse"
		Button ellipse = new Button("Ellipse");
		ellipse.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				outilCourante = new ToolEllipse();
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
			}
		});
		// Button "Image"
		Button image = new Button("Img...");
		image.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				imageAction(stage, stat);
			}
		});
		// Button "Select"
		Button selectionner = new Button("Select");
		selectionner.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				outilCourante = new ToolSelection();
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
			}
		});
		ToolBar toolbar = new ToolBar(box, ellipse, image, selectionner);

		// ToolBar pour couleurs

		// Button rouge
		Button rouge = new Button("Red", new Rectangle(20, 20, Color.RED));
		rouge.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				couleurAction(stat, Color.RED);
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		// Button jaune
		Button jaune = new Button("Yellow", new Rectangle(20, 20, Color.YELLOW));
		jaune.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				couleurAction(stat, Color.YELLOW);
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		// Button vert
		Button vert = new Button("Green", new Rectangle(20, 20, Color.GREEN));
		vert.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				couleurAction(stat, Color.GREEN);
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		// Button gris
		Button gris = new Button("Gray", new Rectangle(20, 20, Color.GRAY));
		gris.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				couleurAction(stat, Color.GRAY);
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		// Button marron
		Button marron = new Button("Brown", new Rectangle(20, 20, Color.BROWN));
		marron.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				couleurAction(stat, Color.BROWN);
				board.draw(canvas.getGraphicsContext2D());
			}
		});
		// Button noir
		Button noir = new Button("Black", new Rectangle(20, 20, Color.BLACK));
		noir.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				couleurAction(stat, Color.BLACK);
				board.draw(canvas.getGraphicsContext2D());
			}
		});

		ToolBar couleurPalette = new ToolBar(rouge, jaune, vert, gris, marron, noir);

		// Canvas
		canvas.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
				press(e);
				draw(canvas.getGraphicsContext2D());
			}
		});
		canvas.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				drag(e);
				draw(canvas.getGraphicsContext2D());
			}
		});
		canvas.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				release(e);
				updateUndoRedoItems();
				draw(canvas.getGraphicsContext2D());
			}
		});
		board.draw(canvas.getGraphicsContext2D());

		Separator sep = new Separator();
		vbox.getChildren().addAll(menubar, toolbar, couleurPalette, canvas, sep, stat);
		Scene scene = new Scene(vbox);
		stage.setScene(scene);
		stage.show();
	}

	private void imageAction(Stage stage, Label stat) {
		stat.textProperty().set("Filled image tool");
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choix d'une image");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
		File selectedFile = fileChooser.showOpenDialog(stage);
		if (selectedFile != null) {
			outilCourante = new ToolImage(selectedFile);
			stat.textProperty().set("Filled image tool. Selected image : " + selectedFile.getAbsolutePath());
		} else {
			stat.textProperty().set("Filled " + outilCourante.getName(this));
		}
	}

	private void couleurAction(Label stat, Color c) {
		if (outilCourante instanceof ToolRect) {
			ToolRect tool = (ToolRect) outilCourante;
			tool.setCouleur(c);
			outilCourante = tool;
			stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
		}
		if (outilCourante instanceof ToolEllipse) {
			ToolEllipse tool = (ToolEllipse) outilCourante;
			tool.setCouleur(c);
			outilCourante = tool;
			stat.textProperty().set("Filled " + outilCourante.getName(EditorWindow.this));
		}
		if (outilCourante instanceof ToolSelection) {
			ToolSelection tool = (ToolSelection) outilCourante;
			tool.changeColors(this, c);
			selection.clear();
			String col;
			if (c == Color.RED)
				col = "red";
			else if (c == Color.YELLOW)
				col = "yellow";
			else if (c == Color.GREEN)
				col = "green";
			else if (c == Color.GRAY)
				col = "gray";
			else if (c == Color.BROWN)
				col = "brown";
			else
				col = "black";
			stat.textProperty().set("Color is changed on " + col);
		}
	}

	private void updateUndoRedoItems() {
		undoMenuItem.setDisable(pileCommande.isUndoEmpty());
		redoMenuItem.setDisable(pileCommande.isRedoEmpty());
	}

	@Override
	public Board getBoard() {
		return board;
	}

	@Override
	public Selection getSelection() {
		return selection;
	}

	@Override
	public CommandStack getUndoStack() {
		return pileCommande;
	}

	public void press(MouseEvent e) {
		outilCourante.press(this, e);
	}

	public void drag(MouseEvent e) {
		outilCourante.drag(this, e);
	}

	public void release(MouseEvent e) {
		outilCourante.release(this, e);
	}

	public void draw(GraphicsContext gc) {
		outilCourante.drawFeedback(this, gc);
	}

	@Override
	public void clipboardChanged() {
		pasteMenuItem.setDisable(Clipboard.getInstance().isEmpty());
	}
}
