package pobj.pinboard.editor.commands;

import java.util.List;

import pobj.pinboard.document.Clip;
import pobj.pinboard.document.ClipGroup;
import pobj.pinboard.editor.EditorInterface;

public class CommandGroup implements Command {
	private EditorInterface editor;
	private ClipGroup group;
	
	public CommandGroup(EditorInterface editor, List<Clip> toGroup) {
		this.editor = editor;
		group = new ClipGroup(toGroup);
	}
	
	@Override
	public void execute() {
		editor.getBoard().removeClip(group.getClips());
		editor.getBoard().addClip(group);
	}

	@Override
	public void undo() {
		editor.getBoard().removeClip(group);
		editor.getBoard().addClip(group.getClips());
	}

}
