package pobj.pinboard.editor.commands;

import java.util.ArrayList;
import java.util.List;

import pobj.pinboard.document.Clip;
import pobj.pinboard.editor.EditorInterface;

public class CommandMove implements Command {
	@SuppressWarnings("unused")
	private EditorInterface editor;
	private List<Clip> toMove = new ArrayList<>();
	private double dx;
	private double dy;
	
	public CommandMove(EditorInterface editor, Clip toMove, double dx, double dy) {
		this.editor=editor;
		this.toMove.add(toMove);
		this.dx = dx;
		this.dy = dy;
	}
	
	public CommandMove(EditorInterface editor, List<Clip> toMove, double dx, double dy) {
		this.editor=editor;
		this.toMove.addAll(toMove);
		this.dx = dx;
		this.dy = dy;
	}
	
	@Override
	public void execute() {
		for(Clip clip : toMove) {
			clip.move(dx, dy);
		}
	}

	@Override
	public void undo() {
		for(Clip clip : toMove) {
			clip.move(-dx, -dy);
		}
	}

}
