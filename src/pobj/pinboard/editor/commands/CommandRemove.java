package pobj.pinboard.editor.commands;

import java.util.ArrayList;
import java.util.List;

import pobj.pinboard.document.Clip;
import pobj.pinboard.editor.EditorInterface;

public class CommandRemove implements Command {
	private EditorInterface editor;
	private List<Clip> toRemove = new ArrayList<>(); 
	
	public CommandRemove(EditorInterface editor, Clip toRemove) {
		this.editor = editor;
		this.toRemove.add(toRemove);
	}
	
	public CommandRemove(EditorInterface editor, List<Clip> toRemove) {
		this.editor = editor;
		this.toRemove.addAll(toRemove);
	}
	
	@Override
	public void execute() {
		editor.getBoard().removeClip(toRemove);
	}

	@Override
	public void undo() {
		editor.getBoard().addClip(toRemove);
	}

}
