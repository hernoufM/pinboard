package pobj.pinboard.editor.commands;

import java.util.ArrayList;
import java.util.List;

import pobj.pinboard.document.Clip;
import pobj.pinboard.document.ClipGroup;
import pobj.pinboard.editor.EditorInterface;

public class CommandUngroup implements Command {
	private EditorInterface editor;
	private List<Clip> clips = new ArrayList<>();
	
	public CommandUngroup(EditorInterface editor, Clip toUngroup) {
		this.editor = editor;
		clips.add(toUngroup);
	}
	
	public CommandUngroup(EditorInterface editor, List<Clip> toUngroup) {
		this.editor = editor;
		clips.addAll(toUngroup);
	}
	
	@Override
	public void execute() {
		for(Clip clip : clips) {
			if(clip instanceof ClipGroup)
			{
				ClipGroup cg = (ClipGroup) clip;
				editor.getBoard().removeClip(cg);
				editor.getBoard().addClip(cg.getClips());
			}
		}
	}

	@Override
	public void undo() {
		for(Clip clip : clips) {
			if(clip instanceof ClipGroup)
			{
				ClipGroup cg = (ClipGroup) clip;
				editor.getBoard().removeClip(cg.getClips());
				editor.getBoard().addClip(cg);
			}
		}
	}

}
