package pobj.pinboard.editor.tools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pobj.pinboard.document.Clip;
import pobj.pinboard.editor.EditorInterface;
import pobj.pinboard.editor.Selection;
import pobj.pinboard.editor.commands.CommandMove;

public class ToolSelection implements Tool {
	private double x;
	private double y;
	private double dx;
	private double dy;
	
	@Override
	public void press(EditorInterface i, MouseEvent e) {
		Selection selection = i.getSelection();
		if(e.isShiftDown())
		{
			selection.toogleSelect(i.getBoard(), e.getX(), e.getY());
		}
		else
		{
			selection.select(i.getBoard(), e.getX(), e.getY());
		}
		x = e.getX();
		y = e.getY();
		dx = e.getX();
		dy = e.getY();
	}

	@Override
	public void drag(EditorInterface i, MouseEvent e) {
		double dxPixel = e.getX()-x;
		double dyPixel = e.getY()-y;
		CommandMove commande = new CommandMove(i, i.getSelection().getContents(), dxPixel, dyPixel);
		commande.execute();
		x = e.getX();
		y = e.getY();
	}

	@Override
	public void release(EditorInterface i, MouseEvent e) {
		dx = e.getX()-dx;
		dy = e.getY()-dy;
		if(!i.getSelection().getContents().isEmpty() && (dx!=0 || dy!=0))
		{
			CommandMove commande = new CommandMove(i, i.getSelection().getContents(), dx, dy);
			i.getUndoStack().addCommand(commande);
		}
	}

	@Override
	public void drawFeedback(EditorInterface i, GraphicsContext gc) {
		i.getBoard().draw(gc);
		i.getSelection().drawFeedBack(gc);
	}

	@Override
	public String getName(EditorInterface editor) {
		return "selection tool";
	}

	public void changeColors(EditorInterface i, Color c) {
		if(c != null)
			for(Clip clip : i.getSelection().getContents()) {
				clip.setColor(c);
			}
	}

}
