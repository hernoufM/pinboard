package pobj.pinboard.editor.tools;

import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import pobj.pinboard.document.ClipImage;
import pobj.pinboard.editor.EditorInterface;
import pobj.pinboard.editor.commands.CommandAdd;

public class ToolImage implements Tool {
	private File filename;
	private double right;
	private double bottom;

	public ToolImage(File filename) {
		this.filename = filename;
	}

	@Override
	public void press(EditorInterface i, MouseEvent e) {
		right = e.getX();
		bottom = e.getY();
	}

	@Override
	public void drag(EditorInterface i, MouseEvent e) {
		right = e.getX();
		bottom = e.getY();
	}

	@Override
	public void release(EditorInterface i, MouseEvent e) {
		Image image = new Image("file:" + filename.getAbsolutePath(), 200.0, 110.0, false, false);
		if (image.getWidth() > right)
			right = image.getWidth();
		if (image.getHeight() > bottom)
			bottom = image.getHeight();
		CommandAdd commande = new CommandAdd(i,
				new ClipImage(right - image.getWidth(), bottom - image.getHeight(), filename));
		commande.execute();
		i.getUndoStack().addCommand(commande);
	}

	@Override
	public void drawFeedback(EditorInterface i, GraphicsContext gc) {
		i.getBoard().draw(gc);
	}

	@Override
	public String getName(EditorInterface editor) {
		return "image tool";
	}

}
