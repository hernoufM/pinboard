package pobj.pinboard.editor.tools;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pobj.pinboard.document.ClipEllipse;
import pobj.pinboard.document.Couleur;
import pobj.pinboard.editor.EditorInterface;
import pobj.pinboard.editor.commands.CommandAdd;

public class ToolEllipse implements Tool {

	private double xInit;
	private double yInit;
	private double x;
	private double y;
	private Couleur couleur = new Couleur(Color.YELLOW);

	@Override
	public void press(EditorInterface i, MouseEvent e) {
		xInit = e.getX();
		x = e.getX();
		yInit = e.getY();
		y = e.getY();
	}

	@Override
	public void drag(EditorInterface i, MouseEvent e) {
		x = e.getX();
		y = e.getY();
	}

	@Override
	public void release(EditorInterface i, MouseEvent e) {
		double left = (x < xInit) ? x : xInit;
		double top = (y < yInit) ? y : yInit;
		double right = (x > xInit) ? x : xInit;
		double bottom = (y > yInit) ? y : yInit;
		if (left != right && top != bottom) {
			CommandAdd commande = new CommandAdd(i, new ClipEllipse(left, top, right, bottom, couleur.toColor()));
			commande.execute();
			i.getUndoStack().addCommand(commande);
		}
	}

	@Override
	public void drawFeedback(EditorInterface i, GraphicsContext gc) {
		double left = (x < xInit) ? x : xInit;
		double top = (y < yInit) ? y : yInit;
		double right = (x > xInit) ? x : xInit;
		double bottom = (y > yInit) ? y : yInit;
		i.getBoard().draw(gc);
		gc.setStroke(couleur.toColor());
		gc.strokeOval(left, top, right - left, bottom - top);
	}

	@Override
	public String getName(EditorInterface editor) {
		String c;
		Color col = couleur.toColor();
		if (col.equals(Color.RED)) 
			c = "red";
		else if (col.equals(Color.YELLOW))
			c = "yellow";
		else if (col.equals(Color.GREEN))
			c = "green";
		else if (col.equals(Color.GRAY))
			c = "gray";
		else if (col.equals(Color.BROWN))
			c = "brown";
		else
			c = "black";
		return c + " ellipse tool";
	}

	public void setCouleur(Color c) {
		couleur = new Couleur(c);
	}
}
