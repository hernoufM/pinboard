package pobj.pinboard.editor;

import java.util.ArrayList;
import java.util.List;

import pobj.pinboard.editor.commands.Command;

public class CommandStack {
	private List<Command> undo = new ArrayList<>();
	private List<Command> redo = new ArrayList<>();
	
	public void addCommand(Command cmd) {
		undo.add(0,cmd);
		redo.clear();
	}
	
	public void undo() {
		Command cmd = undo.get(0);
		undo.remove(0);
		cmd.undo();
		redo.add(0,cmd);
	}
	
	public void redo() {
		Command cmd = redo.get(0);
		redo.remove(0);
		cmd.execute();
		undo.add(0,cmd);
	}
	
	public boolean isUndoEmpty() {
		return undo.isEmpty();
	}
	
	public boolean isRedoEmpty() {
		return redo.isEmpty();
	}
}
