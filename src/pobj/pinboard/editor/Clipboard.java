package pobj.pinboard.editor;

import java.util.ArrayList;
import java.util.List;

import pobj.pinboard.document.Clip;

public class Clipboard {
	private static Clipboard clipboard = new Clipboard();
	private List<Clip> clips = new ArrayList<>();
	private List<ClipboardListener> listeners = new ArrayList<>();
	private Clipboard() { }
	
	public static Clipboard getInstance() {
		return clipboard;
	}
	
	public void copyToClipboard(List<Clip> clips) {
		for(Clip clip : clips) {
			this.clips.add(clip.copy());
		}
		informerListeners();
	}
	
	public List<Clip> copyFromClipboard(){
		List<Clip> clips = new ArrayList<>(this.clips.size());
		for(Clip clip : this.clips) {
			clips.add(clip.copy());
		}
		return clips;
	}
	
	public void clear() {
		clips.clear();
		informerListeners();
	}
	
	public boolean isEmpty() {
		return clips.isEmpty();
	}
	
	public void addListener(ClipboardListener listener) {
		if(!listeners.contains(listener))
			listeners.add(listener);
	}
	
	public void removeListener(ClipboardListener listener) {
		listeners.remove(listener);
	}
	
	private void informerListeners() {
		for(ClipboardListener listener : listeners) {
			listener.clipboardChanged();
		}
	}
}
