package pobj.pinboard.document;

import java.io.Serializable;

import javafx.scene.paint.Color;


@SuppressWarnings("serial")
public class Couleur implements Serializable{
	private double red;
	private double blue;
	private double green;

	public Couleur(Color c) {
		red = c.getRed();
		blue = c.getBlue();
		green = c.getGreen();
	}

	public Color toColor() {
		return Color.color(red, green, blue);
	}
}
