package pobj.pinboard.document;

import java.io.File;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

@SuppressWarnings("serial")
public class ClipImage extends AbstractClip {
	private File filename;
	private transient Image img;
	
	public ClipImage(double left, double top, File filename) {
		super(left, top, left, top, Color.WHITE);
		this.filename=filename;
		img=new Image("file:"+filename.getAbsolutePath(), 200.0, 110.0, false, false);
		setGeometry(left, top, img.getWidth()+left, img.getHeight()+top);
	}
	
	@Override
	public void draw(GraphicsContext ctx) {
		ctx.drawImage(img, getLeft(), getTop(), getRight()-getLeft(), getBottom()-getTop());
	}

	@Override
	public Clip copy() {
		return new ClipImage(getLeft(), getTop(), new File(filename.getAbsolutePath()));
	}
	
	public void recalculerImage() {
		img=new Image("file:"+filename.getAbsolutePath(), 200.0, 110.0, false, false);
	}
}
