package pobj.pinboard.document;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

@SuppressWarnings("serial")
public class ClipGroup extends AbstractClip implements Composite {
	private List<Clip> clips;

	public ClipGroup() {
		super(0.0, 0.0, 0.0, 0.0, Color.WHITE);
		clips = new ArrayList<>();
	}

	public ClipGroup(List<Clip> clips) {
		super(0.0, 0.0, 0.0, 0.0, Color.WHITE);
		this.clips = new ArrayList<>(clips);
		recalculerRectangleEnglobant();
	}

	private void recalculerRectangleEnglobant() {
		double left = Double.POSITIVE_INFINITY;
		double top = Double.POSITIVE_INFINITY;
		double right = 0.0;
		double bottom = 0.0;
		for (Clip clip : clips) {
			if (clip.getLeft() < left)
				left = clip.getLeft();
			if (clip.getTop() < top)
				top = clip.getTop();
			if (clip.getRight() > right)
				right = clip.getRight();
			if (clip.getBottom() > bottom)
				bottom = clip.getBottom();
		}
		super.setGeometry(left, top, right, bottom);
	}
	
	@Override
	public List<Clip> getClips() {
		return clips;
	}

	@Override
	public void addClip(Clip toAdd) {
		if (!clips.contains(toAdd))
			clips.add(toAdd);
		recalculerRectangleEnglobant();
	}

	@Override
	public void removeClip(Clip toRemove) {
		clips.remove(toRemove);
		recalculerRectangleEnglobant();
	}

	@Override
	public void setGeometry(double left, double top, double right, double bottom) {
		move(left - getLeft(), top - getTop());
	}

	@Override
	public void move(double dx, double dy) {
		for (Clip clip : clips) {
			clip.move(dx, dy);
		}
		super.move(dx, dy);
	}

	@Override
	public void draw(GraphicsContext ctx) {
		for (Clip clip : clips) {
			clip.draw(ctx);
		}
	}

	@Override
	public Clip copy() {
		if(!clips.isEmpty())
		{
			List<Clip> copy = new ArrayList<>();
			for (Clip clip : clips)
				copy.add(clip.copy());
			return new ClipGroup(copy);
		}
		return new ClipGroup();
	}

}
